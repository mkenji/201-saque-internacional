package com.itau.saqueinternacional;

import java.util.HashMap;

public class RatesResponse {
	public boolean success;
	public int timestamp;
	public String base;
	public String date;
	public HashMap<String, Double> rates;
	
}

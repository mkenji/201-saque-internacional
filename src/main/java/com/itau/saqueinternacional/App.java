package com.itau.saqueinternacional;

import java.util.HashMap;

import javax.swing.text.html.HTMLDocument.Iterator;

import org.omg.CORBA.Request;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	
    	String response = ApiRequester.request();
    	RatesResponse ratesResponse = JsonConverter.fromJson(response);
    	
    	FileManager.loadFiles("accounts.csv");
    	FileManager.loadFiles("withdrawals.csv");
    	FileManager.loadCustomersObject();
    	System.out.println(FileManager.customers[0].getName());

    	
    	for (String values : FileManager.getMapWithdraws().values()) {
    		String[] itens = values.split(",");
    		double balance = Calculator.convert(Double.parseDouble(itens[1]), itens[2], ratesResponse);
    		Calculator.accountWithdraw(itens[0], balance);
    	}
    	
    	
    }
}

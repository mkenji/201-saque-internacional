package com.itau.saqueinternacional;

import com.github.kevinsawicki.http.HttpRequest;

public class ApiRequester {
	private static String endPoint = "http://data.fixer.io/api/latest?";
	private static String accessCode = "access_key=adca3badaa8fb6889ae0a7c2a66102a1";
	
	static String request() {
		String response = HttpRequest.get(endPoint+accessCode).body();
		return response;
	}
	
}

package com.itau.saqueinternacional;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;

public class FileManager {

	protected static HashMap<String, String> mapAccounts = new HashMap<String, String>();
	protected static HashMap<String, String> mapWithdraws = new HashMap<String, String>();
	public static Customer[] customers = new Customer[FileManager.getMapAccounts().size()];

	public static boolean loadFiles(String nameFile){
		Path pathOrigem = Paths.get(nameFile);
		ArrayList<String> itemsFile = null;

		try {
			itemsFile = (ArrayList<String>) Files.readAllLines(pathOrigem);
			for(int i=1; i<itemsFile.size(); i++) {
				String[] itens = itemsFile.get(i).split(",");
				if(nameFile == "accounts.csv"){
					mapAccounts.put(itens[1], itemsFile.get(i));
				} else {
					mapWithdraws.put(itens[1], itemsFile.get(i));
				}				

			}	
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static void loadCustomersObject() {
		
    	int count = 0;
    	for(String values : FileManager.getMapAccounts().values()) {
    		String[] itens = values.split(",");
    		boolean premium = false;
    		if(itens[4] == "premium") {
    			premium = true;
    		}
    		customers[count] = new Customer(itens[0], itens[1], itens[2], itens[3], premium);
    		count++;
    	}
	}


	public static HashMap<String, String> getMapAccounts() {
		return mapAccounts;
	}
	
	public static HashMap<String, String> getMapWithdraws() {
		return mapWithdraws;
	}
}


package com.itau.saqueinternacional;

public class Calculator {

	static double convert(double amount, String currency, RatesResponse ratesResponse) {
		Double rate = ratesResponse.rates.get(currency);
		return amount/rate;
	}
	
	static boolean accountWithdraw(String username, double balance){
		String itemsAccounts = FileManager.getMapAccounts().get(username);
		String[] itens = itemsAccounts.split(",");	
		double valorSolicitado = Integer.parseInt(itens[3]);
		if(valorSolicitado > balance) {
			itens[3] = String.valueOf(valorSolicitado - balance);
			String output = String.format("%s,%s,%s,%s,%s", itens[0], itens[1], itens[2],itens[3],itens[4]);
			FileManager.getMapAccounts().put(username,output);
			return true;
		}

		return false;
	}

	
}

package com.itau.saqueinternacional;

public class Customer {
	public Customer(String name, String username, String password, String balance, boolean premium) {
		super();
		this.name = name;
		this.username = username;
		this.password = password;
		this.balance = balance;
		this.premium = premium;
	}
	protected String name;
	protected String username;
	protected String password;
	protected String balance;
	protected boolean premium;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getBalance() {
		return balance;
	}
	public void setBalance(String balance) {
		this.balance = balance;
	}
	public boolean getPremium() {
		return premium;
	}
	public void setPremium(boolean premium) {
		this.premium = premium;
	}
	
	
	
}

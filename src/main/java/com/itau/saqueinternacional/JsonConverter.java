package com.itau.saqueinternacional;

import com.google.gson.Gson;

public class JsonConverter {
	
	public static RatesResponse fromJson(String response) {
		Gson gson = new Gson();
		RatesResponse ratesResponse = gson.fromJson(response, RatesResponse.class);
		return ratesResponse;
	}
}
